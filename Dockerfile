FROM ubuntu:18.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y ansible openssh-client rsync
ADD setup-ssh.sh /

RUN mkdir /ansible
add roles /ansible/roles
add ansible.cfg /ansible/ansible.cfg
add setup-eaas.yaml /ansible/setup-eaas.yaml
run mkdir /ansible/inventories
workdir /ansible
